#!/usr/bin/env iocsh.bash
# -----------------------------------------------------------------------------
# PACE-5000 startup cmd
# -----------------------------------------------------------------------------
require(pace5000)

# -----------------------------------------------------------------------------
# Environment variables
# -----------------------------------------------------------------------------
#epicsEnvSet("IPADDR",   "172.30.244.78")
#epicsEnvSet("IPADDR",   "127.0.0.1")
epicsEnvSet("IPADDR",   "192.168.1.33")
epicsEnvSet("IPPORT",   "5025")
epicsEnvSet("LOCATION", "E03; $(IPADDR)")
epicsEnvSet("SYS",      "SE-PS01:")
epicsEnvSet("DEV",      "SE-PACE5000-001")
epicsEnvSet("PREFIX",   "$(SYS)$(DEV)")

# -----------------------------------------------------------------------------
# loading databases
# -----------------------------------------------------------------------------
# iocstats databases
iocshLoad("$(iocstats_DIR)/iocStats.iocsh")

# PACE 5000
iocshLoad("$(pace5000_DIR)pace5000.iocsh", "PREFIX=$(PREFIX), P=$(SYS), R=$(DEV):, IPADDR=$(IPADDR), IPPORT=$(IPPORT)")
